#!/bin/bash

#docker-compose up -docker

case $1 in
mysql)

  docker-compose -f docker-compose-mysql.yml up -d

  Message="MySQL is available under http://localhost:3306"
  Admin="PhpMyAdmin is available under http://localhost:8080"
  ;;
postgre)

  docker-compose -f docker-compose-postgre.yml up -d

  Message="PostgreSql is available under http://localhost:5432"
  Admin="PgAdmin is available under http://localhost:8080"
  ;;
*)
  Message="Well, you haven't gave me anything to run! Please give me either 'mysql' and 'postgre' as argument."
  Admin=""
  ;;
esac

echo ""
echo $Message
echo $Admin
# Why this project?

it's just a play-around with docker and databases, which I personally use for the DBS lecture.

# Which databases will be installed?

I used both **MySQL** and **Postgres**, so I added them here.

# What should I have before using this thing?

You have to install [Docker](https://www.docker.com).

## Docker on Ubuntu:
[Documentation for Docker](https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-docker-engine---community-1)

### docker-compose

copy and paste the following codes into your terminal:

First:
```
sudo curl -L "https://github.com/docker/compose/releases/download/1.23.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
```

After it was completed:
```
sudo chmod +x /usr/local/bin/docker-compose
```

## Docker on Mac and Windows:

Just easily follow the instructions written on the following page:

[Documentation](https://hub.docker.com/?overlay=onboarding)

**Usually this installer also brings the `docker-compose` into your computer**

# Setup

First, you have to clone the project:

```
git clone git@git.thm.de:sasd50/dbs-mysql-postgre-setup.git
```

And then, `cd` or change directory into the folder:

```
cd dbs-mysql-postgre-setup
```

# How can I run my database?

in principle, all you have to do is to give the docker-compose file to the docker-compose command like

```
docker-compose -f your-file.yml
```

what the `run.sh` file does, is the same!

So you have two options to run the databases

## using the pure docker-compose

run the following commands into your terminal:

### MySQL

```
docker-compose -f docker-compose-mysql.yml up -d
```

### Postgres

```
docker-compose -f docker-compose-postgre.yml up -d
```

## using the `run.sh`

you can either give `mysql` or `postgre` to the script:

```
sh ./run.sh mysql
```

or

```
sh ./run.sh postgre
```

That's it!

# How can I log into Admin panels?
MySql has PhpMyAdmin and Postgre has PgAdmin. For using them:

| Database and Application        | Host           | Username  |    Password    |
| ------------- |:-------------:| -----:| ----:|
| MySQL      | http://localhost:3306 | root | my-secret-pw |
| PhpMyAdmin      | http://localhost:8080      |   root | my-secret-pw |
| Postgre | http://localhost:5432      |    postgre | my-secret-pw
| PgAdmin | http://localhost:8080      |    hi@example.com | my-secret-pw